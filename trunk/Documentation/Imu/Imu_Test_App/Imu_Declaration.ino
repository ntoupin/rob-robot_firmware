Imu_t Imu =
{
  .Enable = FALSE,
  .Name = "IMU #1",
  .Sync_Byte = 0x55,
  .Execution_Time = 0,
  .Accelerometer =
  {
    .Enable = TRUE,
    .Name = "Accelerometer",
    .Command = 0x51,
    .Units = "g",
    .X_Value = 0,
    .Y_Value = 0,
    .Z_Value = 0,
  },
  .Angular_Velocity =
  {
    .Enable = TRUE,
    .Name = "Angular Velocity",
    .Command = 0x52,
    .Units = "deg/g",
    .X_Value = 0,
    .Y_Value = 0,
    .Z_Value = 0,
  },
  .Angle =
  {
    .Enable = TRUE,
    .Name = "Angle",
    .Command = 0x53,
    .Units = "deg",
    .X_Value = 0,
    .Y_Value = 0,
    .Z_Value = 0,
  },
  .Magnetometer =
  {
    .Enable = FALSE,
    .Name = "Magnetometer",
    .Command = 0x00,
    .Units = "mG",
    .X_Value = 0,
    .Y_Value = 0,
    .Z_Value = 0,
  },
};
