typedef struct
{
  boolean Enable;
  String Name;
  const byte Command;
  String Units;
  float X_Value;
  float Y_Value;
  float Z_Value;
} Accelerometer_t;

typedef struct
{
  boolean Enable;
  String Name;
  const byte Command;
  String Units;
  float X_Value;
  float Y_Value;
  float Z_Value;
} Angular_Velocity_t;

typedef struct
{
  boolean Enable;
  String Name;
  const byte Command;
  String Units;
  float X_Value;
  float Y_Value;
  float Z_Value;
} Angle_t;

typedef struct
{
  boolean Enable;
  String Name;
  const byte Command;
  String Units;
  float X_Value;
  float Y_Value;
  float Z_Value;
} Magnetometer_t;

typedef struct
{
  boolean Enable;
  String Name;
  const byte Sync_Byte;
  float Execution_Time;
  Accelerometer_t Accelerometer;
  Angular_Velocity_t Angular_Velocity;
  Angle_t Angle;
  Magnetometer_t Magnetometer;
} Imu_t;
