#include "Imu_Definition.h"

#define TRUE 1
#define FALSE 0
#define BYTE_QTY 10

extern Imu_t Imu;
byte Receive_Buffer[BYTE_QTY];

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
}

void loop()
{
  Receive_Serial(&Imu);
  //Print_Values(&Imu);
}

void Receive_Serial(Imu_t *Imu_Ptr)
{
  if (Serial1.available() > 0)
  {
    unsigned long Timestamp = micros();

    // Wait for syncronisation flag
    while ((byte)Serial1.read() != Imu_Ptr->Sync_Byte);

    // Wait to have BYTE_QTY bytes into buffer
    while (Serial1.available() < BYTE_QTY);

    // Fill buffer
    for (int i = 0; i < BYTE_QTY; ++i)
    {
      Receive_Buffer[i] = (byte)Serial1.read();
    }

    // Parse data
    if (Receive_Buffer[0] == Imu_Ptr->Accelerometer.Command)
    {
      if (Imu_Ptr->Accelerometer.Enable == TRUE)
      {
        Imu_Ptr->Accelerometer.X_Value = (int(Receive_Buffer [2] << 8 | Receive_Buffer [1])) / 32768.0 * 16;
        Imu_Ptr->Accelerometer.Y_Value = (int(Receive_Buffer [4] << 8 | Receive_Buffer [3])) / 32768.0 * 16;
        Imu_Ptr->Accelerometer.Z_Value = (int(Receive_Buffer [6] << 8 | Receive_Buffer [5])) / 32768.0 * 16;
      }
      else
      {
        Imu_Ptr->Accelerometer.X_Value = 0;
        Imu_Ptr->Accelerometer.Y_Value = 0;
        Imu_Ptr->Accelerometer.Z_Value = 0;
      }
    }

    if (Receive_Buffer[0] == Imu_Ptr->Angular_Velocity.Command)
    {
      if (Imu_Ptr->Angular_Velocity.Enable == TRUE)
      {
        Imu_Ptr->Angular_Velocity.X_Value = (int(Receive_Buffer [2] << 8 | Receive_Buffer [1])) / 32768.0 * 2000;
        Imu_Ptr->Angular_Velocity.Y_Value = (int(Receive_Buffer [4] << 8 | Receive_Buffer [3])) / 32768.0 * 2000;
        Imu_Ptr->Angular_Velocity.Z_Value = (int(Receive_Buffer [6] << 8 | Receive_Buffer [5])) / 32768.0 * 2000;
      }
      else
      {
        Imu_Ptr->Angular_Velocity.X_Value = 0;
        Imu_Ptr->Angular_Velocity.Y_Value = 0;
        Imu_Ptr->Angular_Velocity.Z_Value = 0;
      }
    }

    if (Receive_Buffer[0] == Imu_Ptr->Angle.Command)
    {
      if (Imu_Ptr->Angle.Enable == TRUE)
      {
        Imu_Ptr->Angle.X_Value = (int(Receive_Buffer [2] << 8 | Receive_Buffer [1])) / 32768.0 * 180;
        Imu_Ptr->Angle.Y_Value = (int(Receive_Buffer [4] << 8 | Receive_Buffer [3])) / 32768.0 * 180;
        Imu_Ptr->Angle.Z_Value = (int(Receive_Buffer [6] << 8 | Receive_Buffer [5])) / 32768.0 * 180;
      }
      else
      {
        Imu_Ptr->Angle.X_Value = 0;
        Imu_Ptr->Angle.Y_Value = 0;
        Imu_Ptr->Angle.Z_Value = 0;
      }
    }

    unsigned long Delta = micros() - Timestamp;
    Imu_Ptr->Execution_Time = (float)Delta / 1000;
  }
}

void Print_Values(Imu_t *Imu_Ptr)
{
  Serial.print(Imu_Ptr->Accelerometer.Name);
  Serial.print(" : ");
  Serial.print(Imu_Ptr->Accelerometer.X_Value); Serial.print(" "); Serial.print(Imu_Ptr->Accelerometer.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Accelerometer.Y_Value); Serial.print(" "); Serial.print(Imu_Ptr->Accelerometer.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Accelerometer.Z_Value); Serial.print(" "); Serial.print(Imu_Ptr->Accelerometer.Units);

  Serial.print(" / ");
  Serial.print(Imu_Ptr->Angular_Velocity.Name);
  Serial.print(" : ");
  Serial.print(Imu_Ptr->Angular_Velocity.X_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angular_Velocity.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Angular_Velocity.Y_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angular_Velocity.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Angular_Velocity.Z_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angular_Velocity.Units);

  Serial.print(" / ");
  Serial.print(Imu_Ptr->Angle.Name);
  Serial.print(" : ");
  Serial.print(Imu_Ptr->Angle.X_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angle.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Angle.Y_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angle.Units); Serial.print(", ");
  Serial.print(Imu_Ptr->Angle.Z_Value); Serial.print(" "); Serial.print(Imu_Ptr->Angle.Units);

  Serial.print(" / ");
  Serial.print("Imu receiving time : ");
  Serial.print(Imu_Ptr->Execution_Time);
  Serial.println(" ms");
}
