/*
   @file Tasks.ino
   @author Nicolas Toupin
   @date 2020-04-10
   @brief Definition of all the tasks
*/

#include <NT_Task_Manager.h>

Task_t Task_Leds;

void Configure_Tasks()
{
  Task_Leds.Configure(500, Tasks_Leds);
}
void Init_Tasks()
{
  Task_Leds.Init();
}

void Deinit_Tasks()
{
  Task_Leds.Deinit();
}
