/*
   @file Pwm.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Definitions for pwms functions.
*/

#include <NT_Pwm.h>

Pwm_t Pwm1;
Pwm_t Pwm2;
Pwm_t Pwm3;
Pwm_t Pwm4;

void Configure_Pwms()
{
  Pwm1.Configure(0, 0, 0, 255, 0, 255);
  Pwm2.Configure(0, 0, 0, 255, 0, 255);
  Pwm3.Configure(0, 0, 0, 255, 0, 255);
  Pwm4.Configure(0, 0, 0, 255, 0, 255);
}

void Attach_Pwms()
{
  Pwm1.Attach(6);
  Pwm2.Attach(5);
  Pwm3.Attach(10);
  Pwm4.Attach(9);
}

void Init_Pwms()
{
  Pwm1.Init();  
  Pwm2.Init();
  Pwm3.Init();
  Pwm4.Init();
}

void Tasks_Pwms()
{
  Pwm1.Set(Motor1.Value1_Op);
  Pwm2.Set(Motor1.Value2_Op);
  Pwm3.Set(Motor2.Value1_Op);
  Pwm4.Set(Motor2.Value2_Op);
}

void Deinit_Pwms()
{
  Pwm1.Deinit();
  Pwm2.Deinit();
  Pwm3.Deinit();
  Pwm4.Deinit();
}
