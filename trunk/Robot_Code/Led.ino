/*
   @file Led.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Definitions for motors functions.
*/

#include <NT_Led.h>

Led_t Led1;

void Configure_Leds()
{
  Led1.Configure(FALSE);
}

void Attach_Leds()
{
  Led1.Attach(13);
}

void Init_Leds()
{
  Led1.Init();
}

void Tasks_Leds()
{
  Led1.Toggle();
}

void Deinit_Leds()
{
  Led1.Deinit();
}
