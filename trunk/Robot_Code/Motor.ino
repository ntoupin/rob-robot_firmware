/*
   @file Motor.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Definitions for motors functions.
*/

#include <NT_Motor.h>

Motor_t Motor1;
Motor_t Motor2;

void Configure_Motors()
{
  Motor1.Configure(512, 0, 0, 1023, -100, 100);
  Motor2.Configure(512, 0, 0, 1023, -100, 100);
}

void Init_Motors()
{
  Motor1.Init();  
  Motor2.Init();
}

void Tasks_Motors()
{
  if (Controller_State == TRUE)
  {
    Motor1.Set(Data.Joy_Ana_Right_Y);
    Motor2.Set(Data.Joy_Ana_Left_Y);
  }
  else if (Controller_State == FALSE)
  {
    Motor1.Set(512);
    Motor2.Set(512);
  }
  else
  {
    // Do nothing
  }
}

void Deinit_Motors()
{
  Motor1.Deinit();
  Motor2.Deinit();
}
