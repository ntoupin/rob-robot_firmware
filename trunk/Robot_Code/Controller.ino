/*
   @file Controller.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Definitions for leds functions.
*/

#include <EasyTransfer.h>

EasyTransfer Transfer;

bool Controller_State = FALSE;
long Last_Timestamp = -1000;

struct Data_Structure_t
{
  int16_t Buttons;
  int16_t Joy_Ana_Left_X;
  int16_t Joy_Ana_Left_Y;
  int16_t Joy_Ana_Right_X;
  int16_t Joy_Ana_Right_Y;
};

Data_Structure_t Data;

void Configure_Controller()
{

}

void Init_Controller()
{
  Serial1.begin(9600);
  Transfer.begin(details(Data), &Serial1);
}

void Tasks_Controller()
{
  // Receive data
  if (Transfer.receiveData())
  {
    Last_Timestamp = millis();    
  }

  // Disable controller if last packet delta is more than 300 ms
  if ((millis() - Last_Timestamp) < 300)
  {
    Controller_State = TRUE;
  }
  else
  {
    Controller_State = FALSE;
  }
}

void Deinit_Controller()
{

}
