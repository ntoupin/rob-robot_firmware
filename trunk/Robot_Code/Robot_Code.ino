/*
   @file Robot_Code.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Main of robot execution
*/

#include <NT_Task_Manager.h>

#define TRUE 1
#define FALSE 0

extern Task_t Task_Leds;
extern Task_t Task_Pwms;
extern Task_t Task_Motors;
extern Task_t Task_Controller;

void setup()
{
  // Configure
  Configure_Leds();
  Configure_Motors();
  Configure_Pwms();
  Configure_Controller();
  Configure_Tasks();

  // Attach
  Attach_Leds();
  Attach_Pwms();

  // Init
  Init_Leds();
  Init_Motors();
  Init_Pwms();
  Init_Controller();
  Init_Tasks();

  // Serial debug
  Serial.begin(9600);
}

void loop()
{
  // Refresh LED
  Task_Leds.Run();
  
  // Run all tasks
  Tasks_Controller();
  Tasks_Motors();
  Tasks_Pwms();
}
